package company.tothepoint.graphqlsecuritypoc.model;

public class Game {

    private String title;
    private String genre;
    private int ageRestriction;

    public Game(String title, String genre, int ageRestriction) {
        this.title = title;
        this.genre = genre;
        this.ageRestriction = ageRestriction;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public int getAgeRestriction() {
        return ageRestriction;
    }
}
