package company.tothepoint.graphqlsecuritypoc.service;

import company.tothepoint.graphqlsecuritypoc.model.Game;
import company.tothepoint.graphqlsecuritypoc.repo.GameRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {

    private GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public List<Game> getGamesWithGenre(String genre) {
        return gameRepository.findByGenre(genre);
    }

    public Game getGameWithTitle(String title) {
        return gameRepository.findByTitle(title);
    }

    public List<Game> getGamesForAgeGreaterThan(int age) {
        return gameRepository.findByAgeGreaterThan(age);
    }

    public Game addGame(Game game) {
        return gameRepository.addGame(game) ? game : null;
    }

    public String removeGame(String title) {
        return gameRepository.removeGame(title) ? title : null;
    }
}
