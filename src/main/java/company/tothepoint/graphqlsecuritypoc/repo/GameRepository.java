package company.tothepoint.graphqlsecuritypoc.repo;

import company.tothepoint.graphqlsecuritypoc.model.Game;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class GameRepository {

    private List<Game> games;

    public GameRepository() {
        games = new ArrayList<>();
        games.add(new Game("Mortal Kombat", "Fighter", 16));
        games.add(new Game("Steet Fighter II", "Fighter", 12));
        games.add(new Game("Tetris", "Puzzle", 6));
        games.add(new Game("Bomberman", "Puzzle", 8));
        games.add(new Game("Broforce", "Shooter", 14));
    }

    public List<Game> findAll() {
        return games;
    }

    public List<Game> findByGenre(String genre) {
        return games.stream()
                .filter(g -> g.getGenre().equals(genre))
                .collect(Collectors.toList());
    }

    public Game findByTitle(String title) {
        return games.stream()
                .filter(g -> g.getTitle().equals(title))
                .findFirst()
                .orElse(null);
    }

    public List<Game> findByAgeGreaterThan(int age) {
        return games.stream()
                .filter(g -> g.getAgeRestriction() > age)
                .collect(Collectors.toList());
    }

    public boolean addGame(Game game) {
        if (findByTitle(game.getTitle()) == null)
            return games.add(game);
        else
            return false;
    }

    public boolean removeGame(String title) {
        if (findByTitle(title) != null)
            return games.removeIf(g -> g.getTitle().equals(title));
        else
            return false;
    }
}
