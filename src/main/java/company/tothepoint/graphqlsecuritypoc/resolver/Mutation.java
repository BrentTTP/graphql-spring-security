package company.tothepoint.graphqlsecuritypoc.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import company.tothepoint.graphqlsecuritypoc.model.Game;
import company.tothepoint.graphqlsecuritypoc.service.GameService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
public class Mutation implements GraphQLMutationResolver {

    private GameService gameService;

    public Mutation(GameService gameService) {
        this.gameService = gameService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Game addGame(String title, String genre, int age){
        Game newGame = new Game(title, genre, age);
        return gameService.addGame(newGame);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String removeGame(String title) {
        return gameService.removeGame(title);
    }
}
