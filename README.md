# Spring Boot GraphQL Security with AOP

In this PoC I used Aspect Oriented Programming (AOP) to secure the single graphql
endpoint with method level restrictions. 

The idea behind this comes from the fact that GraphQL only supports a single endpoint
which should, in theory, be accessible by anyone. That doesn't stop us from restricting
certain users from firing certain queries. More on that later...

There are, in this implementation, 3 
roles you can embody: not authenticated, user and admin. Based on these roles, 
certain methods (that sit behind queries and mutations) won't be accessible.
To give an example, the only query you can fire at the endpoint as an
unauthenticated user is `games` which returns a list of all games currently 
existing in the repo. Other actions like adding a game (mutation) are only 
executable as an admin.

## The Schema
For this PoC a simple GraphQL schema was used with a "Game" object that had a
title, a genre and an age restriction.

A few queries were added to give it some functionality, you can for example get 
a list of all games, filter by genre, get a specific title or get games only
playable from a certain age.

Mutations were added as well to be able to add new games or remove existing ones.

## Security Config
This setup requires some configuration (to be found in the config package) but
bear with me. 

First, there is the SecurityConfig class where we could add certain permissions
for certain endpoints. In a GraphQL ecosystem this can't be achieved because there
only exists one endpoint. The idea is then to allow everyone to fire queries but 
not allowing all the queries to be passed. 

In our configuration we add 2 users: user and admin and their respective roles.

## Annotating The Methods
At class level I added a sneaky little annotation `@EnableGlobalMethodSecurity` that
allows us to add a plethora of security restrictions (on method level).

For example, I added certain admin-only functionality (altering the repo) which
we want to be restricted for, of course, admins only. How this works can be found
in the Mutation resolver class. Above each method I added the `@PreAuthorize` annotation
which allows us to specify the role that is allowed to use this method and thereby
fire the associated query.

## AOP Magic
To start of I added the `@EnableAspectJAutoProxy` annotation to the SpringApplication 
class to enable the use of AOP.

How we want AOP to work is:
1. We want every method in our resolver classes to throw an
exception if there is no authentication in the security context
2. Only resolvers in our application should be accounted for otherwise
other GraphQL classes could be affected.
3. We want to exclude certain methods (the unsecured ones)
from the checks.

You can find the magic in the `SecurityGraphQLAspect` class in the config package.
Here we specify what packages, classes, methods, etc. should be checked or otherwise.

To make life easier I also added a custom annotation for methods that don't need checking:
`@Unsecured` these methods then get ignored by the security aspect.

## Testing and Running
For testing purposes I added some dummy methods that require different roles to be accessible.
Additionally you can run some integration tests that cover most, if not all scenarios.

To actually run this application you simply start the Spring Boot Application and go to
`localhost:8080` in your favorite browser (I know it's Internet Explorer, don't try to hide it).
Here you get prompted by a login screen where you can enter either user/user or admin/admin or
ignore it completely and go to the next step which is:
`localhost:8080/graphiql`. Here you can test all queries and mutations to your heart's
content, note that certain queries and/or mutations will return an error if called without the
appropriate role.

## Credits
Written by: GraphQL Guru Brent

Suggested by: GraphQL Guru Assistent Peter

Original: https://mi3o.com/spring-graphql-security/

