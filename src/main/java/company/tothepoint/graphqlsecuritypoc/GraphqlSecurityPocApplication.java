package company.tothepoint.graphqlsecuritypoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class GraphqlSecurityPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlSecurityPocApplication.class, args);
	}
}
