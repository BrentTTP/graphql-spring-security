package company.tothepoint.graphqlsecuritypoc.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import company.tothepoint.graphqlsecuritypoc.config.Unsecured;
import company.tothepoint.graphqlsecuritypoc.model.Game;
import company.tothepoint.graphqlsecuritypoc.service.GameService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {

    private GameService gameService;

    public Query(GameService gameService) {
        this.gameService = gameService;
    }

    @Unsecured
    public List<Game> games() {
        return gameService.getAllGames();
    }

    public List<Game> gamesWithGenre(String genre) {
        return gameService.getGamesWithGenre(genre);
    }

    public Game gameWithTitle(String title) {
        return gameService.getGameWithTitle(title);
    }

    public List<Game> gamesForAgeGreaterThan(int age) {
        return gameService.getGamesForAgeGreaterThan(age);
    }

    public String securedQuery() {
        return "Secured Query";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String adminQuery() {
        return "Admin Query";
    }

    @Unsecured
    public String unsecuredQuery() {
        return "Unsecured Query";
    }
}
