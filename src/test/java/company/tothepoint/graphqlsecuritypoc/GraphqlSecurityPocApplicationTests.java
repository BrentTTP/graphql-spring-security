package company.tothepoint.graphqlsecuritypoc;

import company.tothepoint.graphqlsecuritypoc.resolver.Query;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GraphqlSecurityPocApplicationTests {

	@Autowired
	private Query query;

	@Test
	public void unsecured_query_ok() {
		query.unsecuredQuery();
	}

	@Test(expected = AccessDeniedException.class)
	public void secured_unauthorized_access_throws_exception() {
		query.securedQuery();
	}

	@Test
	@WithMockUser(username = "user")
	public void secured_ok() {
		query.securedQuery();
	}

	@Test(expected = AccessDeniedException.class)
	public void admin_unauthorized_access_throws_exception() {
		query.adminQuery();
	}

	@Test(expected = AccessDeniedException.class)
	@WithMockUser(username = "user")
	public void without_admin_role_throws_exception() {
		query.adminQuery();
	}

	@Test
	@WithMockUser(username = "admin", roles = "ADMIN")
	public void admin_role_ok() {
		query.adminQuery();
	}

}
